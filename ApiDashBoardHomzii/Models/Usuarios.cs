﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Usuarios
    {
        public int IdUsuarios { get; set; }
        public string CodUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public int CodTipoUsuario { get; set; }
        public string Clave { get; set; }
        public int CodCiudad { get; set; }
        public int CodCompania { get; set; }
        public string Correo { get; set; }
    }
}
