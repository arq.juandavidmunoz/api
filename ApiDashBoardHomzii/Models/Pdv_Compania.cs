﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Pdv_Compania
    {
        public int CodPdv_Compania { get; set; }
        public int CodPdv { get; set; }
        public int CodCompania { get; set; }
    }
}
