﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class PuntoVentas
    {
        public int CodPdv { get; set; }
        public string NombrePdv { get; set; }
        public string TelefonoPdv { get; set; }
        public int CodLovalidad { get; set; }
        public int CodCliente { get; set; }
        public string DireccionPdv { get; set; }
        public int CodPotencialNegocio { get; set; }
        public int CodPerfilNegocio { get; set; }

    }
}
