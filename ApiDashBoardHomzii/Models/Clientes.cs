﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Clientes
    {
        public long CodCliente { get; set; }
        public string NombreCliente { get; set; }
        public string TelefonoCliente { get; set; }
    }
}
