﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Localidades
    {
        public int CodLocalidad { get; set; }
        public string Descripcion { get; set; }
        public int CodCiudad { get; set; }
       
    }
}
