﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Companias
    {
        public int CodCompanias { get; set; }
        public string Descripcion { get; set; }
        public int CodLineaNegocio { get; set; }
    }
}
