﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Ciudades
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
