﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class DetalleFactura
    {
        public int IdDetalle { get; set; }
        public string Factura { get; set; }
        public string IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public int CodCompania { get; set; }
        public int ValorProducto { get; set; }
        public int UnidadesVendidas { get; set; }

    }
}
