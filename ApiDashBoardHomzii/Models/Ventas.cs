﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Models
{
    public class Ventas
    {
        public string Factura { get; set; }
        public int CodPDV { get; set; }
        public DateTime FechaPedido { get; set; }
        public int ValorTotalPedido { get; set; }

    }
}
