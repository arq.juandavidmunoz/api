﻿using ApiDashBoardHomzii.Iservices;
using ApiDashBoardHomzii.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiDashBoardHomzii.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private IClientesServices _oClientesServices;

        public ClientesController(IClientesServices oClientesServices)
        {
            _oClientesServices = oClientesServices;
        }

        // GET: api/<ClientesController>
        [HttpGet]
        public async Task<ActionResult<Clientes>> Get()
        {
            try
            {
                var Clientes = await _oClientesServices.GetAll();
                return Ok(Clientes);
            }
            catch (Exception ex)
            {
                return BadRequest(new { error = ex.Message });
            }

            
        }

        // GET api/<ClientesController>/5 
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ClientesController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ClientesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ClientesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
