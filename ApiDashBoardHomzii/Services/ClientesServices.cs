﻿using ApiDashBoardHomzii.Iservices;
using ApiDashBoardHomzii.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace ApiDashBoardHomzii.Services
{
    public class ClientesServices : IClientesServices
    {
        private readonly string _connectionString;


        public ClientesServices(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Dashboar");
        }


        public Task Delete(int clientesID)
        {
            throw new NotImplementedException();
        }

        public Task<Clientes> Get(int clientesID)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Clientes>> GetAll()
        {
            SqlConnection sql = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("GetAllClientes", sql);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            var response = new List<Clientes>();
            try
            {
                             
                await sql.OpenAsync();

                var reader = await cmd.ExecuteReaderAsync();
                while (await reader.ReadAsync())
                {
                    response.Add(MapToValue(reader));
                }

                
            }
            catch (Exception ex)
            {
                string msg = ex.Message;

            }

            return response;
        }


        private Clientes MapToValue(SqlDataReader reader)
        {
            return new Clientes()
            {
                CodCliente = (long)reader["CodCliente"],
                NombreCliente = (string)reader["NombreCliente"],
                TelefonoCliente = (string)reader["TelefonoCliente"]
            };
        }

        public Task<Clientes> insert(Clientes oScliente)
        {
            throw new NotImplementedException();
        }
    }
}
