﻿using ApiDashBoardHomzii.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDashBoardHomzii.Iservices
{
    public interface IClientesServices
    {
        Task<Clientes> insert(Clientes oScliente);
        Task<List<Clientes>> GetAll();
        Task<Clientes> Get(int clientesID);
        Task Delete(int clientesID);
    }
}
